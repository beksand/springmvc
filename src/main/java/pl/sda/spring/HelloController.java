package pl.sda.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
public class HelloController {

    @Autowired
    private IUserModel iUserModel;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model){
        model.addAttribute("date", new Date());
        model.addAttribute("name", iUserModel.getName());
        return "hello";
    }

    @RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.GET)
    public ModelAndView hello(@PathVariable("name") String name){
        iUserModel.setName(name);
        ModelAndView model = new ModelAndView();
        model.addObject("date", new Date());
        model.setViewName("hello");
        model.addObject("name", name);
        return model;
    }
}
