package pl.sda.spring;

public interface IUserModel {

    public String getName();
    public void setName(String name);

}
