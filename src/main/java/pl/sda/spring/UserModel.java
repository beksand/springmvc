package pl.sda.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import pl.sda.spring.IUserModel;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class UserModel implements IUserModel {

    private String name = "Anonymous";


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name= name;
    }
}
